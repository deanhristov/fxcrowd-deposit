import React, { Component } from 'react'
import DuckImage from '../assets/Duck.jpg'
import './HomeView.scss'

export default class HomeView extends Component {
  render() {
    return (
      <div className={'container duck-wrapper'}>
        <img alt='This is a duck, because Redux!' className='duck' src={DuckImage} />
        <h4>Ready for work...!</h4>
      </div>
    )
  }
}
