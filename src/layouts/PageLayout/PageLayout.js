import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './PageLayout.scss'

export default class PageLayout extends Component {

  static displayName = 'PageLayout';

  static propTypes = {
    children: PropTypes.node,
  };

  render () {
    const { children } = this.props;

    return (
      <div className='page-layout__viewport'>
        {children}
      </div>
    )
  }
}
